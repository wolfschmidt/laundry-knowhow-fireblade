<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>About Me</h2>

                    <P>I have more then 30 years experience in all kind of washings and Denim treatments and how to organize it efficient. I built up several laundries in different countries and made them run profitable. Beyond that I establish, develop, re-organize laundries as well as departments of laundries, including Quality Control Sytems.</P>
                    <P>I do not look only into the procedures and systems but of course I support the employees as well.
                        They need to understand and accept the extensions to make it run in the correct way. For that purpose I give also trainings and practices on the floor, in the laundry and teach the Theorie behind it in an easy and understanding way.
                    </P>
                    <p>
                        Since 2018 I am involved in a project for Company Interloop Ltd. Lahore Pakistan. Their goal is to produce after a few 
                        years around 60.000 Jeans per day in a sustainable way. My task is to build up the laundry without using stones and 
                        hand-scraping. Instead of this to use nebulization Systems for stonewash effects and Laser for hand-scraping. 
                        If still necessary to spray Jeans after Laser to obtain nice and clean look, use pp-replacemnets Instead of 
                        Potassiumpermanganate.
                    </p>
                    <p>
                        After making lots of trials in the development area from Interloop, with different auxiliaries from several Chemical 
                        suppliers on different fabrics, we found very good solutions for bulk production. End of 2018 Interloop started bulk 
                        with 5.000 to 10.000pcs a day. From that day on I was responsible to transfer the good results from sample stage to bulk. 
                        Summer 2019 Interloop already at 20.000 Jeans per day: NO STONES, NO HAND-SCRAPING, NO PP-SPRAY!!
                    </p><BR>
                    <H3>Some results of last years tasks</H3><BR>
                    <TABLE WIDTH="100%" BORDER="1" BORDERCOLOR="#1D5C9C" CELLPADDING="4" CELLSPACING="2">
                        <TR bgcolor="#788487">
                            <TD ALIGN="center" WIDTH="100"><FONT COLOR="white">Laundry</FONT></TD>
                            <TD ALIGN="center" WIDTH="400"><FONT COLOR="white">Improvement</FONT></TD>
                            <TD ALIGN="center" WIDTH="100"><FONT COLOR="white">Period</FONT></TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Hamburg, Germany</TD>
                            <TD style="color:#1D5C9C;">Implementation of a bonus system together with staff association and  labour union.</TD>
                            <TD >7 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Silesia, Poland</TD>
                            <TD  style="color:#1D5C9C;">Reduce Labour cost, change conditions of worker contracts, reduce reworks, change recipes non-profit to 28% profit</TD>
                            <TD >11 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Vrancea, Romania</TD>
                            <TD style="color:#1D5C9C;">Establish a Development department, organize equipment, chose and train staff</TD>
                            <TD >4 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Guatemala City, Guatemala</TD>
                            <TD style="color:#1D5C9C;">Finding solutions for using wet- and dry-Ozone in Garment Wet Processing.</TD>
                            <TD >9 weeks</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Transilvania, Romania</TD>
                            <TD style="color:#1D5C9C;">Shorten transfer-time from samples into bulk,	cut cost for water and energy</TD>
                            <TD >5 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Cup Bon, Tunesia</TD>
                            <TD style="color:#1D5C9C;">Decrease chemical cost from 13% down to 9%</TD>
                            <TD >4 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Bukarest, Romania</TD>
                            <TD style="color:#1D5C9C;">Assure measures during bleaching of Lycra-denims</TD>
                            <TD >6 weeks</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Heshan, China</TD>
                            <TD style="color:#1D5C9C;">Establish quality control and reporting system</TD>
                            <TD >3 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Focsani, Romania</TD>
                            <TD style="color:#1D5C9C;">Build up laundry for fashion jeans with daily capacity of 20.000 denim and 10.000 non denim</TD>
                            <TD >12 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Corlu, Turkey</TD>
                            <TD style="color:#1D5C9C;">Provide technical consultancy services in relation to the activities of  product finishing development center</TD>
                            <TD >4 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Lahore, Pakistan</TD>
                            <TD style="color:#1D5C9C;">Improving the productivity in scraping department with 65%</TD>
                            <TD >5 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Lahore, Pakistan</TD>
                            <TD style="color:#1D5C9C;">Establish technology, procedure and equipment in bulk to apply Resin on rigid garments coincidental assure 
                                dimensional accuracy</TD>
                            <TD >6 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Bangkok, Thailand</TD>
                            <TD style="color:#1D5C9C;">Double the outcome of bulk production</TD>
                            <TD >4 months</TD>
                        </TR>
                        <TR style="text-align: left;">
                            <TD >Kardzhali, Bulgaria</TD>
                            <td style="color:#1D5C9C;">Build up team for scraping, pp-spray and all kind of resin/3-D; establish quality control and reporting system; 
                                optimize lay-out and run-through of production space</td>
                            <td>9 month</td>
                        </TR>


                    </TABLE><BR><BR>
                    <P>For further informations feel free to contact me:<BR>
                        <A HREF="mailto:ischmidt@jeanswash.de">ischmidt (at) jeanswash.de</A><BR>
                        or on cell phone 00 49 173 431 41 21</P>
                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>