<?php
/**
 * Created by PhpStorm.
 * User: Packard-Bell
 * Date: 27.08.2019
 * Time: 16:28
 */
include("header.php");
?>
    <section id="home">
        <!--MAIN BANNER AREA START -->
        <div class="banner-area banner-3">
            <div class="overlay dark-overlay"></div>
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 m-auto text-center col-sm-12 col-md-12">
                                <div class="banner-content content-padding">
                                    <!--                            <h5 class="subtitle">A creative agency</h5>-->
                                    <h1 class="banner-title">We craft sustainability</h1>

                                    <p>We provide sustainability for laundries all over the world.
                                        <br>Of course, we also advise you in all other questions of textile processing.
                                    </p>

                                    <a href="nebul.php" class="btn btn-white btn-circled">lets start</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--MAIN HEADER AREA END -->
    </section>
    <!--  ABOUT AREA START  -->
    <section id="intro" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading">
                        <h2>Environmental Science</h2>

                        <p class="lead">The quality of not being harmful to the environment or depleting natural
                            resources, and thereby supporting long-term ecological balance.
                            Sustainability has often been defined as how biological systems endure and remain diverse
                            and productive. But, the 21st-century definition of sustainability goes far beyond these
                            narrow parameters. Today, it refers to the need to develop the sustainable models necessary
                            for both the human race and planet Earth to survive.
                            Sustainability is a balancing act. The United Nation’s 1987 Report of the World Commission
                            on Environment and Development: Our Common Future noted that sustainable development meets
                            the needs of the present without compromising the well-being of future generations.
                            The concept continues to expand in scope. In 2000, the Earth Charter broadened the
                            definition of sustainability to include the idea of a global society “founded on respect for
                            nature, universal human rights, economic justice, and a culture of peace.”
                            To achieve these lofty goals, humans will have to re-examine their policies on:</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5  d-none d-lg-block col-sm-12">
                    <div class="intro-img">
                        <img src="assets/img/banner/why-choose.png" alt="intro-img" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-7 col-md-12 col-sm-12 ">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <div class="intro-box">
                                <span>01.</span>
                                <h4>Environmental protection</h4>

                                <p>Old models of consumption and industrialization will not support the world’s growing
                                    population. If humans wish to have the water, materials and natural resources needed
                                    to thrive, a new approach to living is called for.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <div class="intro-box">
                                <span>02.</span>
                                <h4>Social responsibility</h4>

                                <p>It’s no secret that economic growth and energy have come at the cost of environmental
                                    degradation. In answer to this challenge, sustainability experts are looking at ways
                                    in which we can slow or prevent pollution, conserve natural resources and protect
                                    remaining environments.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <div class="intro-box">
                                <span>03.</span>
                                <h4>Economic practice</h4>

                                <p>A sustainable society is founded on equal access to health care, nutrition, clean
                                    water, shelter, education, energy, economic opportunities and employment. In this
                                    ideal society, humans live in harmony with their natural environment, conserving
                                    resources not only for their own generation, but also for their children’s children.
                                    Each citizen enjoys a high quality of life and there is social justice for all. </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <div class="intro-box">
                                <span>04.</span>
                                <h4>Reference</h4>

                                <p>(from Sustainability Degrees. Find more on www.sustainabledegrees.com)<br>
                                    Watch also www.envirochemie.com (Recover water and heat)</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="intro-cta">
                                <p class="lead ">Still have any question on mind? <a href="#contact"
                                                                                      class="smoth-scroll">Contact
                                        us</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  ABOUT AREA END  -->


<!--    <div id="wash" style="padding: 10px;">-->
<!--        &nbsp;-->
<!--    </div>-->
    <!--  SERVICE PARTNER START  -->
    <section id="wash2" class=" bg-feature">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading text-white">
                        <h4 class="section-title">Full stack garment solution</h4>

                        <p>I do not look only into the procedures and systems but of course I support the employees as
                            well. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  SERVICE PARTNER END  -->

    <!--  WASH AREA START  -->
    <section id="show">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <a href="stonewash.php" title="Read more about Stonewash">
                        <div class="service-img-icon">
                            <img src="assets/images/stonewash_bear.png" alt="Stonewash" class="img-fluid">
                        </div>
                            </a>
                        <div class="service-inner">
                            <h4>Stonewash</h4>

                            <p>Stonewash is the traditional jeans treatment. Up to the 1970ies all the jeans were
                                sold only as greige, unwashed goods.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box ">
                        <a href="bleaching.php" title="Read more about Bleaching">
                        <div class="service-img-icon">
                            <img src="assets/images/bleaching.png" alt="Bleaching" class="img-fluid">
                        </div>
                            </a>
                        <div class="service-inner">
                            <h4>Bleaching</h4>

                            <p>To lighten the jeans, preferably stonewashed goods, there are different bleaching agents
                                possible with slightly different results.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <a href="effectwash.php" title="Read more about Effect wash">
                        <div class="service-img-icon">
                            <img src="assets/images/effect_wash.png" alt="Effect wash" class="img-fluid">
                        </div>
                            </a>
                        <div class="service-inner">
                            <h4>Effect wash</h4>

                            <p>The most famous one is the so called „Moon-wash“ or „Snow-wash“. To obtain the desired
                                look you need about 3 times more stones then kg garments.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="specials" style="padding: 30px;">&nbsp;</div>
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <a href="dry_processes.php" title="Read more about Dry processes">
                        <div class="service-img-icon">
                            <img src="assets/images/dry_process.png" alt="Dry processes" class="img-fluid">
                        </div>
                            </a>
                        <div class="service-inner">
                            <h4>Dry processes</h4>

                            <p>Nowadays we face many options to obtain a used-look effect on jeans. The most common ones will be described below.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <a href="apply_bleach.php" title="Read more about Apply bleach">
                        <div class="service-img-icon">
                            <img src="assets/images/apply_bleach.png" alt="Apply bleach" class="img-fluid">
                        </div>
                            </a>
                        <div class="service-inner">
                            <h4>Apply bleach</h4>

                            <p>Already more than 20 years ago appeared a procedure to make a stonewash garment looking more authentic.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <a href="resins_3d.php" title="Read more about Resins 3D">
                        <div class="service-img-icon">
                            <img src="assets/images/resins3d.png" alt="Resins 3D" class="img-fluid">
                        </div>
                        </a>
                        <div class="service-inner">
                            <h4>Resins 3D</h4>

                            <p>For permanent effects normally a Glyoxalic Resin is used because it contains less Formaldehyde.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  SERVICE AREA END  -->

    <!-- PRICE AREA START  -->
<!--    <section id="pricing" class="section-padding bg-main">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-12 col-sm-12 m-auto">-->
<!--                    <div class="section-heading">-->
<!--                        <h4 class="section-title">Affordable pricing plan for you</h4>-->
<!---->
<!--                        <p>We have different type of pricing table to choose with your need. Check which one is most-->
<!--                            suitble for you and your business purpose. </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col-lg-4 col-sm-6">-->
<!--                    <div class="pricing-block ">-->
<!--                        <div class="price-header">-->
<!--                            <i class="flaticon-start"></i>-->
<!---->
<!--                            <h4 class="price">-->
<!--                                <small>$</small>-->
<!--                                26-->
<!--                            </h4>-->
<!--                            <h5>Monthly pack</h5>-->
<!--                        </div>-->
<!--                        <div class="line"></div>-->
<!--                        <ul>-->
<!--                            <li>5 GB Bandwidth</li>-->
<!--                            <li>Highest Speed</li>-->
<!--                            <li>1 GB Storage</li>-->
<!--                            <li>Unlimited Website</li>-->
<!--                            <li>Unlimited Users</li>-->
<!--                            <li>Data Security and Backups</li>-->
<!--                            <li>24x7 Great Support</li>-->
<!--                            <li>Monthly Reports and Analytics</li>-->
<!--                        </ul>-->
<!---->
<!--                        <a href="#" class="btn btn-hero btn-circled">select plan</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-sm-6">-->
<!--                    <div class="pricing-block ">-->
<!--                        <div class="price-header">-->
<!--                            <i class="flaticon-value"></i>-->
<!---->
<!--                            <h4 class="price">-->
<!--                                <small>$</small>-->
<!--                                46-->
<!--                            </h4>-->
<!--                            <h5>Monthly pack</h5>-->
<!--                        </div>-->
<!--                        <div class="line"></div>-->
<!--                        <ul>-->
<!--                            <li>5 GB Bandwidth</li>-->
<!--                            <li>Highest Speed</li>-->
<!--                            <li>1 GB Storage</li>-->
<!--                            <li>Unlimited Website</li>-->
<!--                            <li>Unlimited Users</li>-->
<!--                            <li>Data Security and Backups</li>-->
<!--                            <li>24x7 Great Support</li>-->
<!--                            <li>Monthly Reports and Analytics</li>-->
<!--                        </ul>-->
<!---->
<!--                        <a href="#" class="btn btn-hero btn-circled">select plan</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-sm-6">-->
<!--                    <div class="pricing-block">-->
<!--                        <div class="price-header">-->
<!--                            <i class="flaticon-idea"></i>-->
<!---->
<!--                            <h4 class="price">-->
<!--                                <small>$</small>-->
<!--                                76-->
<!--                            </h4>-->
<!--                            <h5>Monthly pack</h5>-->
<!--                        </div>-->
<!--                        <div class="line"></div>-->
<!--                        <ul>-->
<!--                            <li>5 GB Bandwidth</li>-->
<!--                            <li>Highest Speed</li>-->
<!--                            <li>1 GB Storage</li>-->
<!--                            <li>Unlimited Website</li>-->
<!--                            <li>Unlimited Users</li>-->
<!--                            <li>Data Security and Backups</li>-->
<!--                            <li>24x7 Great Support</li>-->
<!--                            <li>Monthly Reports and Analytics</li>-->
<!--                        </ul>-->
<!---->
<!--                        <a href="#" class="btn btn-hero btn-circled">select plan</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
    <!-- PRICE AREA END  -->

    <!--  TESTIMONIAL AREA START  -->
<!--    <section id="testimonial" class="section-padding ">-->
<!--        <div class="container">-->
<!--            <div class="row justify-content-center">-->
<!--                <div class="col-lg-8 text-center">-->
<!--                    <div class="mb-5">-->
<!--                        <h3 class="mb-2">Trusted by hundred over years</h3>-->
<!---->
<!--                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis, dignissimos?</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="row">-->
<!--                <div class="col-lg-8 m-auto col-sm-12 col-md-12">-->
<!--                    <div class="carousel slide" id="test-carousel2">-->
<!--                        <div class="carousel-inner">-->
<!--                            <ol class="carousel-indicators">-->
<!--                                <li data-target="#test-carousel2" data-slide-to="0" class="active"></li>-->
<!--                                <li data-target="#test-carousel2" data-slide-to="1"></li>-->
<!--                                <li data-target="#test-carousel2" data-slide-to="2"></li>-->
<!--                            </ol>-->
<!---->
<!--                            <div class="carousel-item active">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-lg-12 col-sm-12">-->
<!--                                        <div class="testimonial-content style-2">-->
<!--                                            <div class="author-info ">-->
<!--                                                <div class="author-img">-->
<!--                                                    <img src="assets/img/author/3b.jpg" alt="" class="img-fluid">-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <p><i class="icofont icofont-quote-left"></i>They is a great platform to-->
<!--                                                anyone like who want to start buisiness but not get right decision. It’s-->
<!--                                                really great placefor new to start the buisness in righ way! <i-->
<!--                                                    class="icofont icofont-quote-right"></i></p>-->
<!---->
<!--                                            <div class="author-text">-->
<!--                                                <h5>Marine Joshi</h5>-->
<!---->
<!--                                                <p>Senior designer</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="carousel-item ">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-lg-12 col-sm-12">-->
<!--                                        <div class="testimonial-content style-2">-->
<!--                                            <div class="author-info ">-->
<!--                                                <div class="author-img">-->
<!--                                                    <img src="assets/img/author/5b.jpg" alt="" class="img-fluid">-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <p><i class="icofont icofont-quote-left"></i>They is a great platform to-->
<!--                                                anyone like who want to start buisiness but not get right decision. It’s-->
<!--                                                really great placefor new to start the buisness in righ way! <i-->
<!--                                                    class="icofont icofont-quote-right"></i></p>-->
<!---->
<!--                                            <div class="author-text">-->
<!--                                                <h5>Marine Joshi</h5>-->
<!---->
<!--                                                <p>Senior designer</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                        <!--  ITEM END  -->
<!---->
<!--                            <div class="carousel-item ">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-lg-12 col-sm-12">-->
<!--                                        <div class="testimonial-content style-2">-->
<!--                                            <div class="author-info ">-->
<!--                                                <div class="author-img">-->
<!--                                                    <img src="assets/img/author/3b.jpg" alt="" class="img-fluid">-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                            <p><i class="icofont icofont-quote-left"></i>They is a great platform to-->
<!--                                                anyone like who want to start buisiness but not get right decision. It’s-->
<!--                                                really great placefor new to start the buisness in righ way!<i-->
<!--                                                    class="icofont icofont-quote-right"></i></p>-->
<!---->
<!--                                            <div class="author-text">-->
<!--                                                <h5>Marine Joshi</h5>-->
<!---->
<!--                                                <p>Senior designer</p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
                           <!--  ITEM END  -->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
    <!--  TESTIMONIAL AREA END  -->

    <!--  PARTNER START  -->
<!--    <section class="section-padding ">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-3 col-sm-6 col-md-3 text-center">-->
<!--                    <img src="assets/img/clients/client01.png" alt="partner" class="img-fluid">-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-sm-6 col-md-3 text-center">-->
<!--                    <img src="assets/img/clients/client06.png" alt="partner" class="img-fluid">-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-sm-6 col-md-3 text-center">-->
<!--                    <img src="assets/img/clients/client04.png" alt="partner" class="img-fluid">-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-sm-6 col-md-3 text-center">-->
<!--                    <img src="assets/img/clients/client05.png" alt="partner" class="img-fluid">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
    <!--  PARTNER END  -->


    <!--  BLOG AREA START  -->
<!--    <section id="blog" class="section-padding bg-main">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-12 col-sm-12 m-auto">-->
<!--                    <div class="section-heading">-->
<!--                        <h4 class="section-title">Latest Blog news</h4>-->
<!---->
<!--                        <div class="line"></div>-->
<!--                        <p>Our blog journey may come handy to build a community to make more effective success for-->
<!--                            business. Latest and trend tricks will help a lot </p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div class="row">-->
<!--                <div class="col-lg-4 col-sm-6 col-md-4">-->
<!--                    <div class="blog-block ">-->
<!--                        <img src="assets/img/blog/blog-1.jpg" alt="" class="img-fluid">-->
<!---->
<!--                        <div class="blog-text">-->
<!--                            <h6 class="author-name"><span>Tips and tricks</span>john Doe</h6>-->
<!--                            <a href="blog-single.html" class="h5 my-2 d-inline-block">-->
<!--                                Best tips to grow your content quality and standard.-->
<!--                            </a>-->
<!---->
<!--                            <p>If you want to grow your content quality and standard you should foolow these tips-->
<!--                                properly voluptatibus.</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-sm-6 col-md-4">-->
<!--                    <div class="blog-block ">-->
<!--                        <img src="assets/img/blog/blog-2.jpg" alt="" class="img-fluid">-->
<!---->
<!--                        <div class="blog-text">-->
<!--                            <h6 class="author-name"><span>Branding</span>john Doe</h6>-->
<!--                            <a href="blog-single.html" class="h5 my-2 d-inline-block">-->
<!--                                Brand your site at top in few minuts.-->
<!--                            </a>-->
<!---->
<!--                            <p>Brand your site at top, boost your audioance corporis facilis animi voluptas alias ex-->
<!--                                saepe quo voluptatibus.</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-4 col-sm-6 col-md-4">-->
<!--                    <div class="blog-block ">-->
<!--                        <img src="assets/img/blog/blog-3.jpg" alt="" class="img-fluid">-->
<!---->
<!--                        <div class="blog-text">-->
<!--                            <h6 class="author-name"><span>Marketing</span>john Doe</h6>-->
<!--                            <a href="blog-single.html" class="h5 my-2 d-inline-block">-->
<!--                                How to become a best sale <br>marketer in a year!-->
<!--                            </a>-->
<!---->
<!--                            <p>Becomeing a best sale marketer is not easy but not impossible too.Need to follow up some-->
<!--                                proper guidance and strategy .</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
    <!--  BLOG AREA END  -->



<?php include("footer2.php");