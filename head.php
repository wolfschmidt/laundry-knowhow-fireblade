<?php
/**
 * Created by PhpStorm.
 * User: Packard-Bell
 * Date: 27.08.2019
 * Time: 16:26
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Laundry know how, sustainability garmin processing, Jeans wash and special treatments">
    <meta name="keywords" content="Sustainability, Jeans,wash,laundry,know,how,garmin,treatment,backstaining,scraping,shrinkage">
    <meta name="author" content="Wolf Schmidt, Schmidt Medienservice">
    <META NAME="Language" CONTENT="English">
    <META NAME="Distribution" CONTENT="Global">
    <META NAME="ROBOTS" CONTENT="ALL">
    <Meta Name="revisit-after" Content="5 days">
    <META NAME="Identifier-URL" CONTENT="http://www.laundry-knowhow-fireblade.com">
    <META NAME="COPYRIGHT" CONTENT="Sustainable Laundry Know How">
    <META NAME="COMMENT" CONTENT="thanx 4 examining my code">
    <META NAME="CONTACTS" CONTENT="ischmidt@jeanswash.de">
    <META NAME="CONTACT" CONTENT="ischmidt@jeanswash.de">
    <META NAME="Author-Corporate" CONTENT="Wolf Schmidt - Schmidt Medienservice">
    <META NAME="Publisher" CONTENT="Sustainable Laundry Know How">
    <META NAME="Publisher-Email" CONTENT="ischmidt@jeanswash.de">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <!--  Icon Font css  -->
    <link rel="stylesheet" href="assets/fonts/flaticon/flaticon.css" type="text/css">
    <!-- FONT AWESOME ICON  -->
    <link rel="stylesheet" href="assets/css/all.css" type="text/css">
    <link rel="stylesheet" href="assets/css/icofont.css" type="text/css">
    <!--  Animate STYLE  -->
    <link rel="stylesheet" href="assets/css/animate.min.css" type="text/css">
    <!--  Theme CUSTOM STYLE  -->
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <!--  RESPONSIVE STYLE  -->
    <link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

    <title>Sustainability - Laundry Know How and Jeans wash</title>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="assets/js/jquery.min.js"></script>
    <!-- contact form  -->
    <script src="assets/js/contact.js"></script>
    <script src="assets/bootstrap/js/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!--  Counter up  -->
    <script src="assets/js/jquery.waypoints.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>

    <script src="assets/js/jquery.easing.1.3.js"></script>
    <!--  wow animation  -->
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/custom.js"></script>

</head>