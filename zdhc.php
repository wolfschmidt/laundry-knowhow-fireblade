<?php include("header.php"); ?>
    <section id="imprint">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading" style="padding-top: 70px;">
                        <h2>ZDHC</h2>

                        <p>
                            The Zero Discharge of Hazardous Chemicals (ZDHC) Foundation has launched the Chemical Module of the ZDHC Gateway. The online search tool will help chemical buyers to choose safer options.</p>

                        <p>It shows to which level (from 0 to 3) a chemical product conforms with the ZDHC Manufacturing Restricted Substances List (MRSL). A high-level rating indicates a high level of confidence that the chemical formulation conforms to the list.</p>

                        <p>At the start of 2018, in addition to eliminating the need for any coordination with ZDHC, the Gateway will be offered to any company within the textile, leather and footwear chain, even those who are not ZDHC signatory brands or affiliates.</p>

                        <p>The Zero Discharge of Hazardous Chemicals (ZDHC) group has updated its roadmap and refined its focus to four areas.</p>
                        <p>Established in 2011, the roadmap included a preliminary work plan towards addressing the challenge of zero discharge by 2020. Subsequent updates have refined and focused ZDHC efforts.</p>
                        <p>These new focus areas are:</p>
                        <ul>
                            <li style="list-style: disc; margin-left: 20px;">Manufacturing Restricted Substances List (MRSL) and implementation conformity – Includes two primary tracks of work, updates to the MRSL and engagement to promote adherence.
                            </li>
                            <li style="list-style: disc; margin-left: 20px;">Research – Engages with academia and the industry to encourage research into the development of safer alternatives and to conduct research on priority chemicals.
                            </li>
                            <li style="list-style: disc; margin-left: 20px;">Audit protocol – Works to harmonise audit tool with Sustainable Apparel Coalition, finalise the audit conformance process and engage industry groups to promote adoption of the audit tools.
                            </li>
                            <li style="list-style: disc; margin-left: 20px;">Wastewater quality – Intends to minimise chemical pollutants discharged into the environment through good process controls and effective chemicals management by developing wastewater quality guidance.
                            </li>
                        </ul>
                        <br>
                        <p>More information is available on ZDHC website <a href="http://www.roadmaptozero.com" target="_blank">www.roadmaptozero.com</a>
                        </p>
                        <BR>
                    </div>
                </DIV>
            </div>
        </div>
    </section>

<?php include("footer2.php"); ?>