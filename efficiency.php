<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Efficiency</h2>

                    <P />Starting from piece development it is necessary to look already here on the way how they will be done to avoid later troubles in mass production.</P>
                    <P />It is important to create a <u>recipe</u> which shows a liquor ratio close to production conditions. For that special sample washers with possible low liquor ratio are needed. In many cases it is also helpful not to run rinse bathes in the sample washers to have nearly same backstaining/redeposition effect of Indigo as in mass production. Usually the sample washers have a drum with a small diameter. For the effects of stonewash the abrasion of the sample washer cannot be high like in a big washer. That’s why it’s necessary to have a high ratio of garment to stones, such as 1:8 or 1:10, 500 gr jeans with 4 to 5 kg stones, depending on the level of abrasion from target.</P>
                    <P />Another critical issue is the <u>bleach</u> phase. For the later transfer it is a must to note down all conditions of the bleach step. It starts with the activity of Hypochlorite but also important is the temperature, the effective runtime, the quantity of used Hypo and of course in how many steps it was bleached. Not to forget: keep one garment as a bleach target by side!</P>
                    <P />In mass production it is an assumption to fill the <u>big washer</u> with maximum load to achieve good efficiency. To run with low liquor ratio saves not only money in water/wastewater consumption but you gain also runtime due to less water fill in and you need also less auxiliaries because the concentration of them is than higher. In many cases the today used fabrics have a water soluble starch and most of this starch you take out by scraping. That means a classical desize is not anymore needed. You can start directly with stonewash or if necessary with a short prewash.</P>
                    <P />To raise the efficiency in <u>manual operations</u> are generally two possibilities. To buy automatic equipment or train the operators proper and provide them with good tools. For the replacement of manual scraping there are different kind of machines available: brush robots, brush tools, Laser, sandblast booths etc. To support the performance of the operators it’s useful to create whiskers etc by using a mould or elastic manikins with creases (MODUS) f.i. Of course you need to sort out and train the operators according to their capabilities. From my experience the usage of vertical manikins have slightly advantage in faster production then horizontal manikins.</P>
                    <P />If we look at the treatment of applying auxiliaries with <u>spray,</u> it is helpful to build a team. One experienced and skilled spray operator has to work on several manikins. He should use his work time almost in spraying garments. That means he needs a team around him to prepare the manikins all time with garments to spray, having all time sufficient spray solution available and clean the manikins to avoid stains, spots etc. Very good light conditions and exhaust systems with a waterfall filter f.i. encourages the performance of spray operator.</P>
                    <P />These are just some small examples how to boost the productivity, there are many other things possible to do. That depends always of the customers you serve as well as of the conditions in your factory. If you show interest to know more about improving please do not hesitate to contact me either by phone or e-mail.</P>

                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>