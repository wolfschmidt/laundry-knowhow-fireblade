<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Apply Bleach</h2>

                    <p />Already more than 20 years ago appeared a procedure to make a stonewash garment looking more authentic. In that time it was just Potassium Permanganate applied by <u>spray</u> on washed garments.</P>
                    <p />Nowadays laundries still use this technology but in most finishes a scraping is done on rigid garments before wash and spray them. 
                    Doing like this the look is much more authentic.  With different concentrations of the Potassium Permanganate and different amount 
                    of sprayed solutions, more or less white effects appear on the sprayed zone. The first chemical reaction between Potassium 
                    Permanganate and the blue Indigo is an Oxidation of the dyestuff and as a result remains brown Manganese dioxide on the pants. 
                    This needs to be washed out, usually done with Bi-Sulfite and a warm bath.</P>
                <p>
                    As we all know Manganese is a heavy metal, the neutralisation of the brown Mangandioxid (MnO2), resulting from working with 
                    Potassium Permanganate (KMnO4), harms the wastewater. It is much better to work with “replacements”. For more information 
                    please klick on the below.
                </p>
                <p><a href="recipe.php" class="btn btn-white btn-circled" style="border: 2px solid #1d5c9c;">Sustainable Wash Recipe</a></p>
                    
                    <P />
                    To obtain an even higher contrast then above described it’s necessary to apply the Potassium Permanganate by 
                    <u>brushing or sponging.</u> Therefor you use different brushes or sponges and apply manually the adjusted Potassium 
                    Permanganate solution, just move over the desired areas with the brush or sponge. Afterwards neutralize as usually.
                </P>
                <video src="assets/movies/sponging.mp4" controls width="100%" autoplay>
                            Your Browser is not aible to play this video.<br/>
                            This video shows the handmade adding Potassium Permanganate.
                    </video>
                    <p />It’s of course also possible to use <u>Hypochlorite instead of Potassium Permanganate</u> solution. Depending on 
                    the fabric it gives a different look. The difficulty in using Hypochlorite is, that you cannot see well where you applied 
                    Hypo and that you don’t know when the bleaching effect of Hypo is finished. That’s why in many cases it’s useful to put 
                    some few grams Potassium Permanganate into the Hypo solution.  To stop the bleaching action of the Hypo in time you 
                    should have a big barrel with water and Bi-sulfite close to the apply-area. Afterwards neutralize as usually.
                </P>


                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>