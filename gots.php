<?php include("header.php"); ?>
    <section id="imprint">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading" style="padding-top: 70px;">
                        <h2>Global Organic Textile Standard</h2>
                        <h4>Version 5.0 from 01. March 2017</h4>

                        <p>
                            The aim of this standard is to define requirements to ensure organic status of textiles,
                            from harvesting of the raw materials, through environmentally and socially responsible
                            manufacturing up to labelling in order to provide a credible assurance to the end
                            consumer.</p>

                        <p>This standard covers the processing, manufacturing, packaging, labelling, trading and
                            distribution of all textiles made from at least 70% certified organic natural fibres. The
                            final products may include, but are not limited to fibre products, yarns, fabrics, garments,
                            fashion textile accessories (carried or worn), textile toys, home textiles, mattresses, and
                            bedding products as well as textile personal care products.</p>

                        <p>As it is to date technically nearly impossible to produce any textiles in an industrial way
                            without the use of chemical inputs, the approach is to define criteria for low impact and
                            low residual natural and synthetic chemical inputs (such as dyestuffs, auxiliaries and
                            finishes) accepted for textiles produced and labelled according to this standard.</p>

                        <p>The standard sets requirements on working and social conditions that are equivalent to those
                            of leading social sustainability standards. Considering that the core function of this
                            standard is verifying and certifying processing of certified organic fibres, where a
                            particularly high level of assurance of labour conditions is needed, applying a compatible
                            specialised social standard of scheme is recommended.
                        </p>

                        <p>Following are some of the main prohibited and restricted inputs:</p>
                        <ul>
                            <li style="list-style: disc; margin-left: 20px;">Aromatic and/or halogenated solvents</li>
                            <li style="list-style: disc; margin-left: 20px;">Flame retardants</li>
                            <li style="list-style: disc; margin-left: 20px;">Chlorinated benzenes</li>
                            <li style="list-style: disc; margin-left: 20px;">Chlorophenols</li>
                            <li style="list-style: disc; margin-left: 20px;">Complexing agents and surfactants (such as all Aps and APEOs)</li>
                            <li style="list-style: disc; margin-left: 20px;">Formaldehyde and other short-chain aldehydes</li>
                            <li style="list-style: disc; margin-left: 20px;">Genetically modified organisms</li>
                            <li style="list-style: disc; margin-left: 20px;">Heavy metals</li>
                        </ul>
                        <br>
                        <p>Complete list at <a href="http://www.global-standard.org" target="_blank">www.global-standard.org</a>
                        </p>
                        <BR>
                    </div>
                </DIV>
            </div>
        </div>
    </section>

<?php include("footer2.php"); ?>