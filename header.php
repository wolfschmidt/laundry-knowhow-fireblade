<?php
/**
 * Created by PhpStorm.
 * User: Packard-Bell
 * Date: 27.08.2019
 * Time: 16:26
 */
include("head.php");
?>
<nav class="navbar navbar-expand-lg fixed-top trans-navigation">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img src="assets/img/logo.png" alt="Sustainable Laundry Know How" title="Sustainable Laundry Know How" class="img-fluid b-logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i class="fa fa-bars"></i>
                </span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="mainNav">
            <ul class="navbar-nav ">
                <li class="nav-item">
                    <a class="nav-link smoth-scroll" href="#" >
                        Home
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#="navbarSustainability" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sustainability
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarSustainability">
                        <a class="dropdown-item " href="#">
                            ZDHC
                        </a>
                        <a class="dropdown-item " href="#">
                            BSCI
                        </a>
                        <a class="dropdown-item " href="#">
                            GOTS
                        </a>
                        <a class="dropdown-item " href="#">
                            RECIPE
                        </a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarWash" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Wash
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarWash">
                        <a class="dropdown-item " href="#">
                            Stonewash
                        </a>
                        <a class="dropdown-item " href="#">
                            Bleaching
                        </a>
                        <a class="dropdown-item " href="#">
                            Effect Wash
                        </a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link smoth-scroll" href="#">Specials</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link smoth-scroll" href="#">Solutions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link smoth-scroll" href="#">Efficiency</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link smoth-scroll" href="about.php">About Me</a>
                </li>
            </ul>
        </div>
    </div>
</nav>