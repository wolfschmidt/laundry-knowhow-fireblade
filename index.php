<?php
/**
 * Created by PhpStorm.
 * User: Packard-Bell
 * Date: 27.08.2019
 * Time: 16:28
 */
include("header.php");
?>
    <section id="home">
        <!--MAIN BANNER AREA START -->
        
        <!--MAIN HEADER AREA END -->
    </section>
    <!--  ABOUT AREA START  -->
    <section id="intro" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading" style="width:40%;">
                        <h2>In deep sorrow</h2>

                        <p class="lead" style="font-size:24px;">we say goodbye to a great person</p>
                        <img src="assets/images/ingo_schmidt.jpg" title="Ingo Schmidt">
                        <p style="font-size:22px;margin-top:15px;">A tragic accident ended his life overnight.</p>
                        <p style="font-size:22px;line-height: 32px;">Ingo Schmidt will be with us forever in our hearts.</p>
                    </div>
                </div>
            </div>
            
<?php include("footer2.php");