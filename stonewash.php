<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Stonewash</h2>

                    <p>
                        Stonewash is the traditional jeans treatment. Up to the 70ies jeans were sold only as
                        greige, unwashed goods. The “right” look appeared only after wearing longer time and several
                        home-washes. The demand for that used look increased much. It could be done today with
                    <ul>
                        <li style="list-style: disc; margin-left: 20px;">Pumice stones</li>
                        <li style="list-style: disc; margin-left: 20px;">Acid enzymes</li>
                        <li style="list-style: disc; margin-left: 20px;">Neutral enzymes</li>
                        <li style="list-style: disc; margin-left: 20px;">Combination</li>
                        <li style="list-style: disc; margin-left: 20px;">No stones / with Nebulization systems</li>                        
                    </ul>
                    <p><a href="nebul.php" class="btn btn-white btn-circled" style="border: 2px solid #1d5c9c;">See how Nebulization works</a></p>
                    </p>
                    <p>Pumice stone is of volcanic origin. It is formed in the beginning phase of a volcanic
                        eruption, still rich of gas, the magma is loosened by the gasses and during the eruption
                        lava is solidified and a porous structure is formed. The pores have got sometimes very sharp
                        edges giving the pumice stones its abrasive character.</p>

                    <p> Depending on the mining field the formation goes back approx. 15-20.000 years. The highest
                        amounts were found in Turkey, Greece, Iceland, Philippines, Indonesia, China, Japan, as well
                        as in Mexico, USA and Equador.</p>

                    <p>
                        About 75% of the pumice stone is Silicium dioxide and ca. 10 – 15% is made of Aluminium
                        dioxide. The rests are Sodium oxide, Calcium oxide, Iron oxide etc.</p>

                    <p>
                        The used look is put in place due to the fact that the cotton yarn is dyed with Indigo only
                        in the exterior part of the thread. The pumice stones remove only the exterior layer of the
                        thread and the un-dyed, white interior part of the fiber appears. With the pumice stones a
                        very nice used look is possible. But it came out some disadvantages in using pumice stones:

                    <ul>
                        <li style="list-style: disc; margin-left: 20px;">Rapid waste of drum plates</li>
                        <li style="list-style: disc; margin-left: 20px;">High amounts of sludge in combination with fiber rests and Indigo pigments</li>
                        <li style="list-style: disc; margin-left: 20px;">Exhausting unloading of machines, cleaning of pockets, loss of time</li>
                    </ul>
                    </p>
                    <video src="assets/movies/belli_washer_destone_Trim.mp4" controls width="100%" autoplay>
                            Your Browser is not aible to play this video.<br/>
                            This video shows the manual destoning after stonewash.
                    </video>
                    <p>It was necessary to make trials without stones => acid enzymes were developed as useful
                        replacement of pumice stones. They work at a pH of 4 -5 and are pretty strong. The
                        disadvantages of the pumice stones could be solved with acid enzymes. They are strong but
                        aggressive and unfortunately other trouble appeared:
                    <ul>
                        <li style="list-style: disc; margin-left: 20px;">Acid enzymes are active in the fiber interior and reduces the fastness to tearing</li>
                        <li style="list-style: disc; margin-left: 20px;">The goods show a much higher Backstaining of blue Indigo (Redeposition)</li>
                    </ul>
                    </p>
                    <p>That’s why neutral enzymes were developed. They work at a neutral pH, 6 – 7 and they are
                        less strong but less aggressive, the tearing fastness is higher and the Backstaining of blue
                        indigo is lower. But the used effect is not the same as with acid enzymes.</p>

                    <p>Nowadays it is useful to work with a combination of neutral enzymes and pumice stones.
                        A standard recipe in bulk for a 100 kg load should look like this:
                    <ul>
                        <li style="list-style: disc; margin-left: 20px;">First step desize and rinse</li>
                        <li style="list-style: disc; margin-left: 20px;">Waterratio 1:5 - Temperature 40 – 45 °C - Time depends on look, 20 to 60 min</li>
                        <li style="list-style: disc; margin-left: 20px;">100 kg pumice stones size 2 - 4 or 3 - 5 cm, 50 % new and 50 % used</li>
                        <li style="list-style: disc; margin-left: 20px;">1.0 % neutral enzymes or 0.3 % concentrate</li>
                        <li style="list-style: disc; margin-left: 20px;">0.5 – 1.0 g/l dispersing agent</li>
                        <li style="list-style: disc; margin-left: 20px;">Rinse, take out stones, rinse again</li>
                    </ul>
                    </p>
                    <BR>
                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>