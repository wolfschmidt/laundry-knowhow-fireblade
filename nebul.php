<?php include("header.php"); ?>
    <section id="imprint">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading" style="padding-top: 70px;">
                        <h2>Core Nebul</h2>
                        <h4>Tonello Core Finishing Technology</h4>
                        <video src="assets/movies/core_nebul.2.mp4" controls width="100%" autoplay>
                            Your Browser is not aible to play this video.<br/>
                            This video shows the process of nebulization.
                        </video>
                        <p>
                            As an example of Nebulization, you see here the Core system from Tonello. Through some special nozzles on top of the door from a washing machine,
                            the water/chemical mixture is sprayed into the washing machine. The mixture is prepared in an outside tank and pumped with compressed air to the
                            nozzles. Through these very fine nozzles and with compressed-air, the mixture is applied into the drum as a “fog”. The drum of the washing machine
                            rotates whenever the Nebulization system works. If the drum stops after a while with a break and rotates than the other way around, Nebulization
                            stops as well. Like this you easily realize a water-ratio of less than 1:1. The necessary amount of chemicals is calculated to the actual load
                            weight and much less then in the traditional washing machine is needed. As the system works without temperature, you safe as well Energy from
                            heating up water. For further details push the button:
                        </p>
                        <p><a href="recipe.php" class="btn btn-white btn-circled" style="border: 2px solid #1d5c9c;">More Information About Nebulization</a></p>
                        <BR>
                    </div>
                </DIV>
            </div>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>