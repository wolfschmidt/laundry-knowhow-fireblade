<?php include("header.php"); ?>
    <section id="imprint">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading" style="padding-top: 70px;">
                        <h2>Sustainable Wash Recipe/Procedure</h2>

                        <p>
                            The best outcome of a sustainable wash is possible if you use the right washing machine for
                            very low water ratio. A very often used standard recipe works with a ratio of 1:4 – 1:7. But
                            with modern washing machines you are able to go down to a water ratio of 1:1 or 1:2. Like
                            this you save already very much water through the cycle. And in addition you don’t need so
                            much energy to heat the water up to a desired temperature. Final the whole process is
                            shorter due to shorter filling time and less heating time. As you have less water, the
                            concentration of the to be used auxiliaries is much higher, by means you do not need that
                            much amount of each auxiliary.</p>
                        <p>
                            <img src="assets/images/tonello_420.jpg" width="100%"></p>
                        <p><img src="assets/images/yilmak_sample_mc.jpg" width="100%"></p>
                        <p>To save again water, it is very useful to install f.i. an injection system to your washing
                            machines. In that way you inject auxiliaries through special nozzles which are in the door
                            of a front loader. The auxiliaries are prepared and add with some water in a tank beside the
                            washing machine. From here a special pump and computer program send the auxiliaries into the
                            washing machine during it is rotating. Sometimes it is useful to let the rotation continue
                            for 20 to 30 min after all auxiliaries are injected to assure an even look of the jeans.
                            Doing so, the LR can be even below 1:1!</p>
                        <img src="assets/images/core_nuzzles.jpg">
                        <p>Whatever is a liquid auxiliary you can inject. From Amylase to Enzymes, Softener, Dyestuffs
                            or bleaching agents. On the market are f.i. Tonello with their core-system and up-system
                            (www.tonello.com) or the e-flow system from Jeanologia. Also Yilmak with their rainforest
                            system (www.yilmak.com.tr) show good results. With bleaching agent it can give also a kind of
                            stone-wash effect, without using stones! If that does not give enough abrasion on a Jeans,
                            it is possible to add inside washing machine other plates for the drum. Those special “no-stone”
                            plates increase the abrasion on a Jeans due to their abrasive coating which is applied on
                            the plates. Synthetic stones or ceramic stones work pretty well in regards of creating abrasion
                            on a Jeans but they destroy the drum of a washing machine much faster then pumice stones. Be
                            careful!</p>
                        <img src="assets/images/no-stone_plates.jpg">
                        <p>To not use pumice stones anymore will save not just water and energy. The waste-water of a
                            laundry will be much easier to clean. As the stones become during wash-cycles smaller and
                            smaller, by means at the end they are down washed to sand, together with water it results
                            dirty mud or sludge. This mud/sludge is full of whatever chemical was in the Jeans before from
                            spinning, weaving, finishing, stitching and washing/dyeing and so on. Before a laundry can
                            clean their waste water and perhaps re-use it, the mud/sludge needs to be set off in a separate
                            sedimentation pool or basin. Depending on the size of stonewash production, you need to take
                            out this mud/sludge daily or weekly, before it becomes hard as concrete. And you must bring that
                            to a special garbage dump, what means you will harm the Environment.</p>

                        <p>But it is also something possible for saving water and energy before you cut, make and wash
                            the garments. There are two systems on the market which treat the denim fabric before it enters
                            the cutting area. The company Matchpoint Textilmaschinenbau GmbH in Mönchengladbach
                            (www.matchpoint-textile.com) treats the fabric with synthetic diamond tapes in that way,
                            that already some Indigo is taken out before the garments enter the laundry. It is not done in an
                            abrasive way, as you could imagine, but in an art of slight touch. Result is that the fabric
                            tear strength does not lose more then 5% (after my experience). Is the fabric treated in
                            such way before cut and make, the laundry just needs to do a rinse-wash or garment-wash and the
                            result looks like it was done a stone-wash. Doing so, you can imagine that an ecological
                            bleach with a replacement of Hypochlorite shows much more light garments as a result than with a
                            standard wash where the fabric was not treated before with this so called diamond denim
                            finish
                            DDF.</p>
                        <img src="assets/images/leg_tubes.jpg">
                        <p>Another method is to treat the fabric before cut and make with Ozone. To convert Oxygen (O2)
                            to Ozone (O3) you need electrical energy. Once you produced enough O3 for treating the fabric,
                            you need to wash out the rest of Ozone from the fabric because too much ppm of Ozone may harm
                            the workers. Therefore it is absolute necessary to have a monitor and alarm system for detecting
                            Ozone. The company Jeanologia in Spain provides you with a complete system
                            (www.jeanologia.com).</p>

                        <p>So far we just talked about washes. But many jeans have of course fashion treatments, mainly
                            done by hand-scraping and spray with Potassium-Permanganate. The resulting brown
                            Manganesedioxide (MnO2) after pp-spray is neutralised or washed-out by Sodium Hydrogen
                            Sulfite (NaHSO3) or other chemicals. These products have an effect on the chemical oxygen demand
                            (COD) and the BOD (biological) of the waste-water. But many chemical companies can provide you
                            also with 100% biodegradable products for neutralising KMnO4.</p>

                        <p>&nbsp;</p>

                        <p>With a modern and strong Laser (Tonello, Jeanologia, VAV Laser www.vavtechnology.com or
                            Seilaser www.seilaser.com) you can reach nowadays very nice used look where is no more chemical spray
                            needed. But depending on the fabric, still a spray with an environmental and GOTS proved
                            product is very helpful. Here you should contact companies such as Dr. Petry in Reutlingen, Germany
                            (www.drpetry.de) or Rudolf Chemie in Geretsried, Germany (www.rudolf.de) or CHT in Tübingen,
                            Germany (www.cht.com) or Garmon in San Marino (www.garmonchemicals.it) or Zaitex in Italy
                            (www.zaitex.com).</p>

                        <p>Last but not least, we have to dry the garments in a dryer or on air. Not a joke, some
                            laundries already move the washed and extracted garments with a hanging system up to the ceiling and
                            take the hot air underneath the roof to pre-dry the garments. Afterwards the garments are put
                            into the dryer for about 15min to get a soft hand-feel.</p>

                        <p>But for those who do not have the option of a very high ceiling or big space underneath the
                            roof, you better use for your steam dryer a system which controls the temperature very
                            exact. But not as usual at the waste-air channel of the dryer or due to humidity control but on the
                            surface of the garments inside the dryer! For that you need a special infra-red probe and a
                            clever computer program to keep the temperature all time on the garments as desired. Using
                            this system (it measures through the infra-red probe the temperature on the surface of the
                            garment) you save first of all easily 25 to 30% of energy because the dryer needs at least 25 to 30%
                            less drying time. By means, you also gain capacity!</p>
                        <img src="assets/images/infra_red.jpg">
                        <p>Final as the temperature is very exact on the garment, you do have nearly no issue with
                            measurements. Because the shrinkage test at the laundry is done in a dryer with the
                            infra-red system as well as later the bulk production. By means you save energy, gain capacity and
                            improve quality! Contact company EKTOS SAS in France, Mr. Thomas Zeyer ( tzeyer@gmail.com ) for more
                            detail info.
                        </p>
                        <BR>
                    </div>
                </DIV>
            </div>
        </div>
    </section>

<?php include("footer2.php"); ?>