<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row" id="top">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Solutions</h2>
                    <p><a href="#yellowing"><button class="btn btn-white btn-circled btn-small">Anti Yellowing</button></a>
                        <a href="#streaks"><button class="btn btn-white btn-circled btn-small">Streaks, Cracks and Stripes</button></a>
                        <a href="#fastness"><button class="btn btn-white btn-circled btn-small">Fastness</button></a>
                        <a href="#elastane"><button class="btn btn-white btn-circled btn-small">Elastane damages</button></a>
                    </p>
                    <h3 id="staining">Staining</h3>
                    <p>In many cases a backstaining or redeposition of blue indigo is undesirable. Either the inside pockets are not pretty white, the cast of the jeans is too blue daubed or the used areas do not show nice “pepper and salt”-contrast, a part of the locker indigo turned back to the cotton and “re-dyed” it.</P>
                    <p>
                    <H4 />What to do to avoid that?</H4>
                    <p>Of course it’s all about the quality of the used fabric but in Garment Wet Processing are some useful solutions to improve the results. One way could be to spray the garments before washing with a special auxiliary to deepen the color and improve the rubbing fastness, such as the Atrix system of CHT-Tübingen, Germany. This product has to be applied by spray and afterwards immediately glazed manual with a brush or rubber gloves. Subsequent put the garments in a curing oven, dry them at 80 to 90 °C and fix them at 150 °C.</p>
                    <p>Additionally to that a fixation of the indigo during washing is possible. Usually fixing agents are cationic products and accumulated to the fiber. Per temperature it will be linked to the fiber. 
                        There are well known fixing agents available such as Lava Fix FF from Dystar. Of course other products are available on the market but normally they are all cationic products and work more or less in the same way as above described. Important is to keep the pH at 6 to 7 all time.</p>
                    <p>These are two options which could be done before the wash process is started. During the several wash steps especially during desizing and stonewash appears a high backstaining/redeposition. 
                        The desizing with an Amylase should not run too long time, in many cases a short desize with 5 to 8 min at 40 °C is enough to clean the garments sufficient and avoid the “Re-dyeing”. 
                        Here it should be applied already very strong detergents together with some “backstaining-blocker”. Some warm and cold rinses afterwards will help for sure. These products must be used during stonewash processes as well. 
                        Sometimes it’s necessary to run stonewash in two or three phases to avoid too long runtime with high backstaining rate. First phase might be shorter like 15 to 25 min and then next phase with a bit 
                        longer runtime as 25 to 35 min and so on and so fort, with rinses in between.</p>
                    <p>The best anti-backstaining-agent chemistry to use nowadays is a combination of the usage of an ethoxylate and polyesterpolyglycol products. Commercially speaking chose from DYSTAR Lava Sperse CPOS together with Lava Sperse KDS 
                        f.i. or from company Dr. Petry the very strong Perlavin JAB new.</p>
                    <p>If the garments are still not clean enough, even after some rewashes with cleaning auxiliaries, a short “bleach” with Laccase 
                        at 50 to 70 °C will help much. Because the Laccase reduces first the locker indigo on the surface of the garments and turns 
                        the cast generally from blue to grey, means the jeans won’t look blue daubed. Hereby it is important to rinse and spin the 
                        jeans well and dry the jeans in a tumbler on the left side (inside out) with high temperature and possible short run time.</p>
                    <p>A very successful method, fast and low cost, is to clean the jeans by using Ozone. The advantage is that you do not need to 
                        put the garments again in a washer, rinse,soften, spin and dry them in a tumbler. Dry garments can be placed in the Ozone 
                        machine and the result is seen immediately. Just let them run once more for a short time in a tumbler at temperature to 
                        destroy the rest of Ozone.</p>
                    <p>Backstaining of Indigo is unavoidable consequence of stoning or bio-stoning. Laccase treatment after these processes is 
                        basically a process for lightening blue denim as well as removal backstaining simultaneously. Accurate control of bleaching 
                        process so as to discriminate between removal of backstaining without effecting denim is impossible. No chemical process 
                        can differentiate between the indigo trapped in the pocket cloth and the indigo in the warp.</p>
                    <p id="yellowing">Here are just a few options shown and short explained. If you are interested for further information please do not 
                        hesitate to contact us either by phone (0049 173 431 41 21) or mail <A HREF="mailto:ischmidt@jeanswash.de" />(ischmidt@jeanswash.de).</A>
                    </p>
                    <p><a href="#top"><button class="btn btn-white btn-circled btn-small" style="float: none;">To Top</button></a></p>
                    <h3>Anti Yellowing</h3>
                    <p>A nature of the Indigo dyestuff is the fast and easy oxidation, its low fastness to light. Thanks to that we can easily do stonebleach and all other kind of fashion treatment on the blue jeans. Unfortunately the ozon in the atmosphere and the power of the sun can do also some “treatments” on the blue jeans. As a result we face yellow-green-grey color changes, especially in summer time. 
                        The ozon in our atmosphere destroys the indigo of the blue jeans and forms some yellow Isatine. Those can be washed out but remains awful white lines, especially at the edges, folders or center creases from the garments.</p>
                    <p>How to avoid that in garment wet processing? Beside the fact that the fastness to light of the chosen fabric is the base, Laundries are able to slow down the process of yellowing but cannot stop it.</p>
                    <ul id="streaks">
                        <li style="list-style: disc; margin-left: 20px;">First of all it is very important to rinse the garments very well. Any locker Indigo at the surface of the blue jeans yellows very fast.</li>
                        <li style="list-style: disc; margin-left: 20px;">In case of bleaching with Hypochlorite make sure that the wash-load is very well neutralized, like one bath with Peroxid followed by a bath with Bi-silfite and a warm plus a cold rinse.</li>
                        <li style="list-style: disc; margin-left: 20px;">Use so called anti-yellowing softener in final step. There are different products on the market available.</li>
                        <li style="list-style: disc; margin-left: 20px;">Well known Anti-Ozone-Softeners are f.i. from Dystar Lava Jeans LHC or PRO 01, from Dr. Petry the Perisoft LOF/R new</li>
                        <li style="list-style: disc; margin-left: 20px;">Make sure that the garments are very well dried and do not have a high rest humidity inside.</li>
                    </ul>
                    <p>&nbsp; </p>
                    <p><a href="#top"><button class="btn btn-white btn-circled btn-small" style="float: none;">To Top</button></a></p>
                    <h3>Streaks, Cracks and Stripes</h3>
                    <p>Up to ca. 10 years ago wash lines, stripes, streaks, cracks etc have been not desired issues at the look of a washed jeans.  Nowadays we try many times to achieve this kind of “bad wash” with all possible options we have. I will give some examples how to avoid the appearance of those lines and also how to create them in a common way.</p>
                    <H4>How to avoid the “bad wash”?</H4>
                    <p>We need to look at the garments already before start washing. Because in many cases the rigid jeans arrive in the laundry irregular packed or fold. If these jeans are stored some days in such conditions it is like they are irregular pressed. Due to the starch on the rigid jeans and this press effect the garments show some hard and irregular edges. At these edges you have in the washing machine higher abrasion, the starch and indigo will be washed/scraped out faster than at other places and finally appear white lines. To avoid that we must press the garments flat before washing, put them on a topper or at least arrange them well before taking into the washing machine.</p>
                    <p>But also nice, flat jeans could show those lines if the washing process is not correct done. First of all it is necessary to have the right speed of the drum. The speed should be chosen in that way that if you stay in front of the washing machine you can see the garments falling down from  “2 o’clock” to “7 o’clock” or the other way around from “10 o’clock” to “5 o’clock”, means diagonal through the drum. Like that is the longest way for the garments to fall down guaranteed , means they are opened again after rolling at the lowest point of the drum and be brought up by the beaters.  In modern stone washing machines it is usually a speed of 23 or 24 to 27 to 28 rpm. If you overload the machine means the garments have not enough space and time for opening during falling down and stripes appear. As a standard load is 25 to 30% of the drum volume seen.</p>
                    <p>The recipe should have a liquor ratio of 1:4 to 1:7, garment weight to water amount, depending on the quality of the fabric and its dyed indigo. Sometimes it’s still necessary to prewash the garments with an Amylase for desizing. Some fabrics still have starch which is made out of corn f.i. and difficult to dilute just with hot water. There are different Amylases for desizing on the market available which work best at 40 °C or 60 °C or even 80 to 90 °C. Not only liquor ratio and temperature are important for using Amylase but also time and pH. Usually the run time for a complete desize is 10 to 20 min and the pH should be between 6 and 8. Also heavy metal ions influence the strength of Amylases and could make desize incomplete. Sometimes it is necessary to catch these ions with an add-on sequest auxiliary.</p>
                    <H4>How to obtain the ”bad wash”?</H4>
                    <p id="fastness">Doing all things opposite then as described above! Run slow speed, overload the machine, don’t use Amylase, press the garments 
                        irregular before washing! Additionally it is helpful to apply resin before irregular pressing to make the jeans even more 
                        stiff and have high abrasion at the edges. Then scrape the edges before washing or put them in a net and wash them crunched. 
                        Or twist them and fix the twisted legs with plastic wires f.i. or put them in tubes for washing.
                    </p>
                    <p><a href="#top"><button class="btn btn-white btn-circled btn-small" style="float: none;">To Top</button></a></p>
                    <h3>Fastness</h3>
                    <p>To improve the rub fastness of fabrics is one of the sensitive tasks in laundry business. Following are some of the possibilities described.</p>
                    <p>Of course it’s all about the quality of the used fabric but in Garment Wet Processing are some useful solutions to improve the fastness. One way could be to spray the garments before washing with a special auxiliary to deepen the color and improve the rubbing fastness, such as the Atrix system of CHT-Tübingen, Germany. This product has to be applied by spray and afterwards immediately glazed manual with a brush or rubber gloves. Subsequent put the garments in a curing oven, dry them at 80 to 90 °C and fix them at 150 °C.</p>
                    <p>Another option is to use special auxiliaries for fixing indigo dyestuff in a bath. Usually fixing agents are cationic products and accumulated to the fiber. Per temperature it will be linked to the fiber. A well known product is the Rotta Fix 1545 from ROTTA/DYSTAR which you apply in a bath with liquor ratio of 1:10 at 30 °C for 3-5 min and then heat up to 60 °C for further more 10-15 min runtime. Of course other products are available on the market but normally they are all cationic products and work more or less in the same way as above described. Important is to keep the pH slightly acid all time during process.</p>
                    <p>This could be done at the beginning of the wash process, even before desize, or at the end of a wash process. If you want to add some softener, it should be done after the fixing agent is applied. Usually the softeners are also cationic products and the fixing agent should reach first the fiber before the softener could occupy these places. But the usage of softener decreases the rub fastness, be aware of that. Especially silicon softener reduces the value of rub fastness much.</p>
                    <p>Some special fixing agents of pigment dyes leave a layer on the fabric which needs to be dried very well at 100 °C in a tumbler to show good results according rub fastness improvement. Make sure that the garments are generally dried well. But from the other side you should clean the washing machine after usage of fixing agents with one rinse before you let the drum be dry and some dried layer of these auxiliaries remain at the drum.</p>
                    <p>Also the hardness of the water can influence the backstaining behavior and there for the rub fastness. If the hardness is high the enzymes and detergents might not work well and as a result the jeans become too blue. It could be useful to run the desize and the stonewash with softwater and the other bathes such as rinses with the normal used water or to work with special ion catcher. It depends on the quality of the existent water.</p>
                    <p id="elastane">Generally warm and cold rinses improve the rub fastness, rinse out as much as possible of the loosen dyestuff is the easiest way to obtain better results. Dry the garments inside out and dry them well, don’t overload the dryer, may be divide the batch in two parts.</p>
                    <p><a href="#top"><button class="btn btn-white btn-circled btn-small">To Top</button></a></p>
                    <h3>Elastane damages</h3>
                    <p>
                        In powerstretch fabrics which have more than 30% elasticity, weft yarns are exposed to a big tension. Moreover, usage of 
                        sensitives fibres as Tencel, Modal etc and chemicals used for bleaching & merserizing triggers puckering issues.</p>
                    <p>
                        The main course of puckering is “moving of elastane in corespun wefts”.</p>
                    <p>
                        The trials and observations of puckering issue shows that the elastane could move up to 2cm inside corespun weft, 
                        especially after washing. There is no elastane breakages in the yarn and there are no big differences in physical 
                        properties of elastane between puckering and normal part. That means, the elastane itself is not damaged but it slips 
                        out of the seam.</p>
                    <p>
                        Nowadays there are “Dual-Core” yarns available which stay much better in the seam. Here the elastane yarn is spun by 
                        cotton as usual but in addition with another material like polyester f.i. Also the seam itself can be very helpful to 
                        keep the elastane yarn. A double lockstitch is less elastic than chain stitches. And extra stitches as bartacks or zigzag 
                        seams hold the elastane and do not allow it to move.</p>
                    <p>
                        Most important is the needle, its thickness and shape/form of the end/top. When a needle moves through the fabric 
                        the yarn should not be hit but do a sidestep. If the needle is too thick the chance to damage the elastane is high and 
                        it will move out of the seam. If the needle is too sharp it will most properly damage the elastane yarn as well. 
                        The are special needles on the market available which are not too thick and have a more round end/top. (R, RRT, SES or SUK).</p>
                    <p>
                        In the laundry it is necessary to use scraping tools with by-pass valves in order to not overstretch the Jeans and 
                        pre-damage the elastane. Same for Laser Dollies and Spray booths. Short washing processes, less temperature and not use 
                        stones do help to keep the elastane yarn inside the seam. The usage of Silicon softener should be avoided, yarn slips 
                        out too fast. 
                    </p>
                    <p><a href="#top""><button class="btn btn-white btn-circled btn-small" style="float: none;">To Top</button></a></p>
                    <BR>
                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>