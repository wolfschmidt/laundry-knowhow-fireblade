<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Bleaching</h2>

                    <p>To lighten the jeans, preferably stonewashed goods, there are different bleaching agents
                        possible with slightly different results refer to the color cast, some are more blueish some
                        are more greyish. The most common once are described below.
                    <ul>
                        <li style="list-style: disc; margin-left: 20px; color: #1D5C9C;">Potassium Permanganate</li>
                        <li style="list-style: disc; margin-left: 20px; color: #1D5C9C;">Sodium Persulphate</li>
                        <li style="list-style: disc; margin-left: 20px; color: #1D5C9C;">Glucose</li>
                        <li style="list-style: disc; margin-left: 20px; color: #1D5C9C;">Laccase</li>
                        <li style="list-style: disc; margin-left: 20px; color: #1D5C9C;">Ozon / sustainable bleach</li>
                        <li style="list-style: disc; margin-left: 20px; color: #1D5C9C;">Peroxide</li>
                        <li style="list-style: disc; margin-left: 20px; color: #1D5C9C;">Sodium Hypochlorite</li>
                    </ul>
                    </p>
                    <p>
                        The bleaching with <span style=" color: #1D5C9C;">Potassium Permanganate</span> is a bit easier then with Hypo because it is a
                        powder and doesn’t lose strength as long as you keep the container closed. Due to this you
                        are able to obtain continuously the same outcome, as long as you keep the same conditions in
                        terms of load weight, water ratio, temperature, run time, pH and grams of <span style=" color: #1D5C9C;">Potassium
                            Permanganate.</span> During the bleaching process do not arise acids, the goods are not so much
                        stressed then with Hypo. But the blue cast is very grey and the garments become very flat.
                        As a result of the chemical reaction Manganese Dioxide is formed which needs to be
                        eliminated by neutralization with f.i. Na-Bisulfit, after the bleaching process is finished.
                        Usually bleaching is done with the water ratio at 1:8 to 1:10, the pH slightly acid at 5 –
                        6, the temperature not higher than 40 °C. Latest after 15 min the process is finished and
                        neutralization should be done for about 10 min.
                    </p>

                    <p>
                        A rather weak bleaching agent, however with good reproducibility and good contrast is Sodium
                        Persulphate. It is necessary to work at high temperature (80 °C) and with high amounts (at
                        least 6 g/l) to see a lighten effect. In many cases you need to repeat the procedure 2 or 3
                        times. The cast is bit greyish but not as much as with <span style=" color: #1D5C9C;">Potassium Permanganate</span>. Water ration
                        should be 1:7.
                    </p>

                    <p>
                        A stronger bleaching effect than with <span style=" color: #1D5C9C;">Sodium Persulphate</span> you obtain with <span style=" color: #1D5C9C;">Glucose</span>. It has
                        also a quit good reproducibility and gives a grey cast, less than with Potassium
                        Permanganate but more than with <span style=" color: #1D5C9C;"><span style=" color: #1D5C9C;">Sodium Persulphate</span></span>. For good results you need about 10 – 15
                        g/l <span style=" color: #1D5C9C;">Glucose</span> and to run at high temperature (80 °C) and with a high amount of alkali, at
                        least 15 ml/l NaOH. Water ratio 1:7 and for ca. 30 min, then rinse warm and cold.
                        The glucose bleaching process is a reductive bleaching with glucose as reducing agent.
                        <span style=" color: #1D5C9C;">Glucose</span> converts at strong alkaline pH values and temperatures of at least 80 °C (mostly
                        carried out at 90–95 °C) the indigo pigment into the water soluble leuco indigo, which can
                        strip easily from the fiber. In the bleaching bath a strong dispersing agent is needed, so
                        that the leuco indigo does not dye the white weft yarn. Due to the oxygen in the drum
                        washing machine the soluble leuco form converts back to the insoluble indigo pigment. This
                        pigment form is drained with the bleaching bath.<br>
                        The bleaching process with glucose was promoted for long time as the preferred ecological
                        form of bleaching, however, this process requires much energy due to high temperature, and
                        the waste water has a high pH value and a very high COD load (chemical oxygen demand).
                    </p>

                    <p>
                        A good bleaching effect is also possible if you use <span style=" color: #1D5C9C;">Laccase</span>. Sometimes the process must be
                        repeated for 2 or 3 times but it is easy to handle. As it is an enzyme it keeps it strength
                        as long as you keep the container closed and in not too hot or too humid conditions. It has
                        a very good ecology and no waste water charge. It doesn’t damage the fiber and is very well
                        suited for elastane fibers. As it bleaches just the Indigo pigment and no other dyestuff,
                        the resulting cast is more grey-blue color. The <span style=" color: #1D5C9C;">Laccase</span> oxidizes Indigo to soluble
                        compounds. Usually the purchasable <span style=" color: #1D5C9C;">Laccase</span>s contain a buffer system to keep always the
                        correct pH. The water ratio should be 1:4 to 1:6, Temperature 50 – 70 °C, pH 4 – 6 and the
                        run time 20 – 30 min, perhaps several times.
                    </p>

                    <p>
                        In many above described cases the responsible bleaching atom is the active oxygen. So, why
                        not to use it directly, f.i. out of <span style=" color: #1D5C9C;">Ozon</span>. <span style=" color: #1D5C9C;">Ozon</span> is a very reactive gas which gives one of its
                        three oxygen atoms very easy free for further reactions. This free and high reactive oxygen
                        atom “bleaches” the Indigo. Usually you need a generator to produce out of f.i. compressed
                        air the <span style=" color: #1D5C9C;">Ozon</span> gas. This gas needs to be discharged into a washer with jeans. The goods can be
                        wet or dry. If the garments are wet the bleaching effect is stronger, faster. The obtained
                        color cast is also a bit more greyish than bleached by Hypo. Hypo bleaches everything:
                        Indigo, other dyestuffs and the white yarn as well. That’s the difference!
                    </p>
                    <p>
                        Nearly all suppliers for <span style=" color: #1D5C9C;">sustainable bleaching</span> products are using a kind of organic Peroxide. Just company CHT in Tübingen, 
                        Germany and company ACTICELL in Austria using different sources, as far as I know by today. Even CHT has a patent on their 
                        products/procedure by means it is outstanding. These products are all green screened and do not harm the wastewater as f.i. 
                        bleaching with Hypo or KMnO4. But the look from all of the sustainable products is a bit more greyish than bleached by Hypo. 
                        To obtain very light Jeans, it is necessary to increase product and run-time. With some fabrics you need to re-wash/clean 
                        the garments after ecological bleaching procedure. That makes it more expensive then bleaching with Hypo. This is still 
                        the big challenge for Chemical Suppliers! But it works, especially as a combination of sustainable products and Ozone/Ozone 
                        in water. Very helpful is if you treat the fabric before cutting with the Diamond Denim Fish (DDF). This machine from 
                        company Matchpoint Textilmaschinenbau in Mönchengladbach, Germany takes out the Indigo from the surface of a Denim 
                        Fabric by using synthetic diamonds.<br>
                        Here is a video from a running machine (without fabric) just to demonstrate no vibration, 
                        by placing a coin on it: 
                    </p>
                    <video src="assets/movies/DDF_machine.mp4" controls width="100%">
                            Your Browser is not aible to play this video.<br/>
                            This video shows the how less the vibrations are.
                    </video>
                    <p>
                        Bleaching with <span style=" color: #1D5C9C;">Peroxide</span> is especially used on black Denim to obtain a nice clear and bright
                        grey color. For low temperature bleaching with <span style=" color: #1D5C9C;">Peroxide</span> like 50 °C and pH 9 – 10 use complex
                        builder to soft water and remove any foreign ions such as Fe. Use <span style=" color: #1D5C9C;">Peroxide</span> activator from
                        any supplier and work in high water ratio like 1:10 with slow rotation to avoid bleach
                        escapes.<br>
                        For high temperature bleach at 90 – 95 °C and pH 12 – 13 you need same but instead of
                        activator you need stabilizer! If you do so the oxygen will not escape too fast but working
                        on the garment. The best and cheap stabilizer is after my experience Na2SiO3 (Waterglass).
                        TAED (Tetra-Acetyl-Ethylene-Diamine) is also a very good stabilizer. It is usually used
                        along with Detergent and can be obtained in granulated form. It can enable bleaching right
                        down to ~ 70 °C. Or use any organic stabilizer from supplier.
                    </p>

                    <p>
                        Some jeans have a bottoming with Sulfur dyes, especially with Sulfur Black. This is the most
                        important dyestuff for black denim and also used for topping or bottoming of dark indigo
                        blue dyeing. But not all bleaching agents have the same effect in bleaching Sulfur dyes. If
                        the bleaching process only affects the indigo dye, black or grey shades will be seen.
                    </p>

                    <p>
                        Hydrogen peroxide is a chemical compound, which reacts as an oxidizing agent. Bleaching with
                        hydrogen peroxide is typically chosen for denim articles whenever a high whiteness is
                        requested. Hydrogen peroxide dissociates only slightly in an aqueous medium. The free acid
                        has only a very weak bleaching action. For this reason, hydrogen peroxide bleaching is
                        carried out exclusively in alkaline medium. If the hydrogen peroxide solution is activated
                        with alkali, then the dissociation equilibrium is moved to the right and the concentration
                        of perhydroxy anions is increased:</p>

                    <p align="center">
                        <img src="assets/images/peroxid.jpg"></p>

                    <p>
                        Stabilization of the hydrogen peroxide is of fundamental importance to achieve a uniform
                        bleaching effect. Moreover, for bleaching in hard water the use of a complexing agent is
                        required. Otherwise catalytic damages may cause small holes in the garment.
                    </p>

                    <p>
                        The bleaching process can be stopped by 1-2 rinse baths and a neutralization of the alkali
                        process with acetic acid. But residues of peroxide can cause catalytic damages. By using
                        catalase the processor is on the safe side. Catalase is a common enzyme, which catalyzes the
                        decomposition of two molecules of hydrogen peroxide to water and oxygen.
                    </p>

                    <p>
                        Bleaching with Hypochlorite is the most cheap way of lighten blue jeans and it is the
                        general way how to do it. You can find it all over the world and it gives a pretty “sky
                        blue” cast. To neutralize the arising acids during the bleaching process, it is useful to
                        work with NaOH and a pH around 12. Otherwise the tear strength is going down too much.
                        Sodium hypochlorite is a powerful oxidizing agent with a corresponding high redox potential.
                        Hypochlorite consequently reacts relatively unselective, it reacts even with the fibers. The
                        danger of fiber damage with this bleaching agent is much greater compared to other bleaching
                        processes. Due to its high oxidation agent potential, it is more save to run the bleaching
                        at lower temperature (40 – 45 °C). After obtaining the desired bleach effect, a
                        neutralization of the Hypo rest is necessary. This is done usually in an oxidative way (with
                        <span style=" color: #1D5C9C;">Peroxide</span>) plus a reductive way (with Na- Bisulfit). The water ratio for bleach bathes should
                        be 1:7 or 1:8, the temperature not higher than 50 °C and the amount of Hypo chosen referring
                        to the desired look. All over a bath should not run longer than 20 min. When the desired
                        look is achieved the first neutralization of the rest Hypo should be done with <span style=" color: #1D5C9C;">Peroxide</span>
                        adding it into the same bath and let the machine run for another 3 – 5 min. Then rinse cold
                        and start the second neutralization with cold water and Na-Bisulfit for another 5 – 10 min,
                        rinse again. The disadvantage of Hypo is, that when you open the container it loses strength
                        during the time, especially in summer time (hot and bright). That makes it for the Laundry
                        Manager difficult to choose always the correct amount of Hypo. He must check the bleaching
                        continuously by watching the garments.
                    </p>

                    <p>
                        The actual bleaching agent in bleaching liquors is not sodium hypochlorite (NaOCl) itself,
                        but the hypochlorous acid (HOCl) which is formed from it in a pH sensitive equilibrium. In
                        order to ensure mild bleaching conditions, a pH range between 10 - 12 has been found as
                        optimum range for effective bleaching. Since, under practical conditions, pH levels below 10
                        are difficult to keep constant, an initial pH range between 11.0-12.5 is selected, as a rule
                        to take into account the consumption of soda ash or caustic soda. The control of the pH
                        value is very important. The strongest bleaching activity is at pH 10 – 12, at pH 5 – 7 the
                        activity is the lowest.
                        Even this process is very cost-effective, however, a major disadvantage is that the AOX
                        content, a measure of organic halogen compounds, often exceeds the permitted effluent
                        pollution.
                    </p>

                    <p>
                        But the “too grey” garments can be made again a bit more blueish with following recipe:<br>
                        1.0g/l NaOH + 3.0-5.0 g/l H2O2 + 1.0 g/l Dispergent<br>
                        Water ratio 1:7 | Temperature 70 – 80 °C | run time 5 – 15 min<br>
                        Rinse several times
                    </p>
                    <BR>
                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>