<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Dry Processes</h2>

                    <p>Nowadays we face many options to obtain a used-look effect on jeans. The most common ones will be described below.</p>
                    <p><u>Sandblast</u> is one of the oldest method to have a used area on the jeans. Sandblast is banned since some years and none of 
                        the serious companies use that treatment anymore due to high impact of human health of operators! <u>Scraping</u> with abrasive 
                        paper is done.<br>
                        Normally also on rigid garments. Scraping needs to have as an important tool a manikin. Whether it is horizontal or 
                        vertical installed depends on the desired finishes. The vertical manikins have a slight advantage because the operator can 
                        see easily left and right intensity of his scraping work. These differences of intensity from left and ride you avoid 
                        best in using <u>brush robots,</u> such as the E4 or E5 from TONELLO, Italy.<br>
                        Brush robots work best on 100% cotton garments, with any elastic jeans it is a bit difficult. If you do scraping on 
                        manikins, horizontal or vertical doesn’t matter, you must install a by-pass valve. The set of a defined pressure inside 
                        the rubber tubes assures blasting of seams or any other damage of the Jeans and helps to keep measurements correct.
                    </p>
                    
                    <p>To produce <u>whiskers and moustaches</u> etc for obtaining an authentic look, very high skilled people are necessary. 
                        For these people are different tools on the market to support the artistic success of their efforts.<br>
                        As it is very difficult to educate many humans in producing day by day with same quality a lot of garments with whiskers 
                        etc it’s useful to think about moulds f.i. <u>Moulds</u> are made of a kind of rubber which 
                        shoemaker f.i. uses. Out of this material you form a “negative” which you place inside the jeans. Then go over it 
                        with the abrasive paper and as a result appear nice whiskers. The company MODUS in Italy provides 
                        laundries with moulds made of elastic material which you blow up on a manikin and then scrape over it completely. 
                        Much more precise with high reproducibility as manual work or moulds is the newest technology of <u>Laser.</u>
                    </p>
                    <video src="assets/movies/Lazer_front_back_Trim.mp4" controls width="100%" autoplay>
                            Your Browser is not aible to play this video.<br/>
                            This video shows how the laser works.
                    </video>
                    <p>Nowadays Laser perform nearly perfect if you have the newest technology and very skilled Laser specialist working with 
                        the used computer programs. On some fabrics it is even possible to not use afterwards a spray with 
                        Potassium-Permanganate. On some fabrics you need to use in addition a Booster which helps to bring out the whiteness 
                        of lasered areas. For high performance Laser contact for instance Jeanologia in Spain or Tonello in Italy.
                    </p>
                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>