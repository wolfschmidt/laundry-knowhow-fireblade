<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Effect Wash</h2>

                    <p>
                        The most famous one is the so called „Moon-wash“ or „Snow-wash“. To obtain the desired look
                        you need about 3 times more stones then kg garments. If the stones are used and small the
                        effect is more smooth. Also possible is to use instead of stones pieces of fabrics which are
                        strap with belts or bands. Soak the stones or fabrics for about 10 min in a bath with 20 to
                        40 g/l Potassium Permanganate or Na-Hypochlorite. Extract the stones or fabrics a bit to
                        avoid too big spots. Carry over that in a dry washer, cover the stones with a piece of
                        plastic foil, load jeans and run the machine for 5 to 20 min. As longer it runs the look
                        will be more smooth. Separate the jeans from the stones, neutralize them and go on with
                        other steps as you like.
                    </p>
                    <p>
                        A more efficient way is possible if you have a modern washing machine connected to a Nebulization System. 
                        With such injection system the bleaching agent will be applied through special nozzles into the drum. 
                        With correct calculated product and short run-time the “Random-Bleach” – Effect is seen very well. The advantage 
                        of a Nebulization System is in the way that Random Bleach can be resulted even by Replacements of Potassium-Permanganate. 
                        By means, your waste-water is not harmed and you don’t need stones or any other carrier!  
                    </p>
                    <p>
                        Another nice effect is the so called "Bad-wash". This wash looks like a bad home-wash, means
                        has some stripes and streaks. Therefor you put the garments in a net and start directly
                        stonewash at low liquor ratio. No desize is needed. Sometimes it’s helpful to press the
                        garments irregular before washing. To highlight the lines even more it might be helpful to
                        spray them before net-washing with Resins and make them more stiff. Additionally some
                        scraping on the edges will increase the effect.
                    </p>

                    <p>
                        With the “Bottle-wash” you obtain irregular spots on the jeans. This method is typically
                        used with pigment dyes. Put some plastic bottles, which have some small holes, with
                        different colors together with jeans in a washer and let it run for a certain time. You can
                        use this method on rigid garments, washed or even bleached garments as well as on white
                        jeans.
                    </p>

                    <p>
                        For the “Splash-wash” it’s necessary to use rigid garments. These rigid garments must be
                        placed in a washer with a possible very low liquor ratio. You add a high amount of caustic
                        soda and run it at low speed for some minutes. After process is finished, rinse the garments
                        and then neutralize them with some acetic acid at low temperature.
                    </p>
                    <BR>
                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>