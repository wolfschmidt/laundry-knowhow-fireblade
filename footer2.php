<?php
/**
 * Created by PhpStorm.
 * User: Packard-Bell
 * Date: 27.08.2019
 * Time: 16:29
 */
?>
<section id="footer" >
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-8 col-md-8">
                <div class="footer-widget footer-link">
                    <h4>We take care of your<br>business is successful</h4>
                    <p>The transfer of the look from developments into production, fast and well running, shows the real performance of a laundry.</p>
                </div>
            </div>
            <div class="col-lg-2 col-sm-4 col-md-4">
                <div class="footer-widget footer-link">
                    <h4>About</h4>
                    <ul>
                        <li><a href="imprint.php">Imprint</a></li>
<!--                        <li><a href="#">Service</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Testimonials</a></li>
                        <li><a href="#">Blog</a></li>-->
                    </ul>
                </div>
            </div>

<!--            <div class="col-lg-2 col-sm-6 col-md-6">-->
<!--                <div class="footer-widget footer-link">-->
<!--                    <h4>Quick Links</h4>-->
<!--                    <ul>-->
<!--                        <li><a href="impressum.php">Imprint</a></li>-->
<!--                        <li><a href="#">Support</a></li>-->
<!--                        <li><a href="#">Privacy Policy</a></li>-->
<!--                        <li><a href="#">Report Bug</a></li>-->
<!--                        <li><a href="#">License</a></li>-->
<!--                        <li><a href="#">Terms & Condition</a></li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
            <div class="col-lg-3 col-sm-6 col-md-6">
                <div class="footer-widget footer-text">
                    <h4>Our location</h4>
                    <p class="mail"><span>Mail:</span> ischmidt@jeanswash.de</p>
                    <p><span>Cell Phone :</span>00 49 173 431 41 21</p>
                    <p><span>Location:</span> Burger Landstraße 6, 42659 Solingen, Germany</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="footer-copy">
                    © 2020 Ingo Schmidt. All Rights Reserved. <a href="http://schmidt-medien.de" target="_blank">Design by Schmidt Medienservice</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--  FOOTER AREA END  -->