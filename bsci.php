<?php include("header.php"); ?>
    <section id="imprint">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading" style="padding-top: 70px;">
                        <h2>BSCI</h2>
                        <h4>Business Social Compliance Initiative</h4>

                        <p>
                            The BSCI Code of Conduct aims at setting out the values and principles that members who participate in BSCI strive to implement with their business partners along their supply chains. Each member who participates in a BSCI participant endorses the Code of Conduct when joining.</p>
                        <h5>11 Principles Aiming at the Highest Labour Protection</h5>
                        <p>The BSCI Code of Conduct draws on important international labour standards protecting workers’ rights such as International Labour Organization (ILO) conventions and declarations, the United Nations (UN) Guiding Principles on Business and Human Rights as well as guidelines for multinational enterprises of the Organization for Economic Co-operation and Development (OECD).</p>

                        <p>It sets out 11 core labour rights, which participants and their business partners commit to implementing within their supply chains in a step-by-step development approach.</p>

                        <ul>
                            <li style="list-style: disc; margin-left: 20px;">The Rights of Freedom of Association and Collective Bargaining. The enterprise respects the right of workers to form unions or other kinds of worker’s associations and to engage in collective bargaining.</li>
                            <li style="list-style: disc; margin-left: 20px;">Fair Remuneration. The enterprise respects the right of workers to receive fair remuneration.</li>
                            <li style="list-style: disc; margin-left: 20px;">Occupational Health and Safety. The enterprise ensures a healthy and safe working environment, assessing risk and taking all necessary measures to eliminate or reduce it.</li>
                            <li style="list-style: disc; margin-left: 20px;">Special Protection for Young Workers. The enterprise provides special protection to any workers that are not yet adults.</li>
                            <li style="list-style: disc; margin-left: 20px;">No Bonded Labour. The enterprise does not engage in any form of forced servitude, trafficked or non-voluntary labour.</li>
                            <li style="list-style: disc; margin-left: 20px;">Ethical Business Behaviour. The enterprise does not tolerate any acts of corruption, extortion, embezzlement or bribery.</li>
                            <li style="list-style: disc; margin-left: 20px;">No Discrimination. The enterprise provides equal opportunities and does not discriminate against workers.</li>
                            <li style="list-style: disc; margin-left: 20px;">Decent Working Hours. The enterprise observes the law regarding hours of work.</li>
                            <li style="list-style: disc; margin-left: 20px;">No Child Labour. The enterprise does not hire any worker below the legal minimum age.</li>
                            <li style="list-style: disc; margin-left: 20px;">No Precarious Employment. The enterprise hires workers on the basis of documented contracts according to the law.</li>
                            <li style="list-style: disc; margin-left: 20px;">Protection of the Environment. The enterprise takes the necessary measures to avoid environmental degradation.</li>
                        </ul>
                        <br>
                        <p>For further information go to <a href="http://www.amfori.org" target="_blank">www.amfori.org</a> (what we do)
                        </p>
                        <BR>
                    </div>
                </DIV>
            </div>
        </div>
    </section>

<?php include("footer2.php"); ?>