<?php include("header.php"); ?>
<section id="imprint">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="section-heading" style="padding-top: 70px;">
                    <h2>Resins 3-D</h2>

                    <p>There are many different kind of so called Resins on the market available. For permanent  effects normally a <u>Glyoxalic</u> Resin is used because it contains less Formaldehyde then Melamine Resins. The usage of other Resins such as polyacrylates, polyvinyls, polyurethans, siloxanes or other polymers etc gives different results. In some cases a catalyst for chemical reaction is needed. In nearly all cases you need temperature for the reaction with the fiber, either in a curing oven or with a press. To obtain f.i. a shiny look a special mix of the above mentioned products is needed plus a press with metal plates, high temperature of 180 °C and high pressure of 60 psi.</p>
                    <p>There are plenty of options how to combine the “Resins”  for achieve finishes as crispy touch, glossy look, oily or waxy finish, leather appearance, brilliance shine and so on and so fort. Nearly no limits it seems!</p>
                    <p>The main usage of Resins is nowadays to achieve permanent 3-D effects and/or improve the scraping effect on rigid garments. The percentage of the used Resin depends on that much. For <u>permanent effects</u> it is necessary to have a higher amount of Resin as 20 to 30%. If it is just for <u>better scraping results</u> very often 5 to 10% Resin is sufficient. During stonewash the small amount will be washed off and a bleaching process afterwards is possible. In some cases the usage of just polyacrylates instead of Resins is enough. If not, might be helpful a combination of Resin with polyacrylates or other polymers. The options in which way to go are quit huge and a good knowledge about the chemistry behind the “Resins” is warm welcome.</p>
                    <p>One <u>dangerous</u> risk in applying Resin on cotton pants is that the tear and tensile strength goes down. Not selden the garments tear easily if too much Resin was applied or the temperature in the curing oven was too high. To reduce the loss of the strength it might be successful to minimize the percentage of Resin and add some percentage of polymers and/or silicon f.i. Of course the temperature at the garments during curing should be proved by a surface thermometer.</p>
                    <H2 />How to apply Resins?</H2>
                    <p>Spray, brush, sponge, dip or bath? The advantage of spray, brush or sponge is generally that the Resin is just on the right side of the pants applied. Brushing or sponging is used if you want to apply the resin just locally, like on the back side for forming knee creases. An additional soft-wash will give the pants at least on the left side, the side which touches the skin, a good hand-feel. To apply in a washer has the advantage of high productivity but the disadvantage that the jeans become pretty blue. Backstaining/redeposition of Indigo is very high. A compromise might be dipping the pants in a drum full of Resin. It is faster done then spray and with a less blueish result then from a washer. If you apply in that way normally a smaller amount of Resin then in spray is needed. After each dip of course you need to refill the drum with the desired mix and extract the garments in a centrifuge.</p>
                    <p>For <u>forming</u> the 3-D creases you can find some moustache formers on the market or kind of presses. Also it is possible to form the creases manually by folder the area, fix it with a clam or tag it. If you crumble the garment and put it in a net you will receive nice streaks and stripes after stonewash. If you press the pants irregular before wash you will face as a result pretty nice wash-lines from bottom to hem.</p>
                    <p>These few statements should give you just a little overview about the huge world of Resins and their sense. As you can imagine the range of success or failure is quit big. A deep knowledge about the chemistry of Resins and how to use them is strongly requested.</p>
                    <p>For further-on training of your people and/or support in finding solutions please do not hesitate to contact me.</p>

                </div>
            </DIV>
        </div>
    </div>
</section>

<?php include("footer2.php"); ?>